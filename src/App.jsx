import React, {Component} from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {TextField, FlatButton} from 'material-ui';
// Core
import './App.css';


class App extends Component {
    constructor (props) {
        super (props);
        this.state = {
            query: ''
        }
    }

    search () {
        console.log(this.state.query);
    }

    render () {
        return (
        <MuiThemeProvider>
        <section className='App'>
            <div className='App-title'>
                <h1>Music Master</h1>
            </div>
            <div className='App-search'>
                <TextField
                    hintText="Busca un artista"
                    floatingLabelText="Busqueda"
                    type="text"
                    value={this.state.query}
                    onChange={event => {this.setState({query: event.target.value})}}
                    onKeyPress={event => {
                        if (event.charCode === 13) {
                            this.search();
                        }
                    }}
                />                
                <FlatButton 
                    label="Buscar" 
                    onClick={() => this.search()}
                />
            </div>
            <div className='App-profile'>
                <div>Artist Name</div>
                <div>Artist Photo</div>
            </div>
            <div className='App-gallery'>
                Gallery
            </div>
        </section>
        </MuiThemeProvider >
        );
    }
}

export default App;